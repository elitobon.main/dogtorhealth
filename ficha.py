#Configuración y módulos
import tkinter as tk
import webbrowser
from tkinter import *
from tkinter import ttk 
from tkinter import messagebox as MessageBox
from login import (
    userclass,
)


chat = Tk() 
chat.title("Modulo de Chat")
chat.geometry("1600x900")
chat.resizable(width=False, height=False)
chat.config(bg='white')

marco_primario_chat = Frame(chat)
marco_primario_chat.place(x=0,y=0)
marco_primario_chat.config(chat,width="1600", height="120")
marco_primario_chat.config(bg='#ACE0D4')
    
marco_superior = Frame(chat)
marco_superior.place(x=0,y=0)
marco_superior.config(chat, width="350", height="900")
marco_superior.config(bg='#DEF3EE')
logo = PhotoImage(chat, file="images/logo3.png")
fondo = Label(image=logo, borderwidth=0).place(x=15,y=0) 

def FichaClinica():
    ficha=Toplevel()
    ficha = Tk() 
    ficha.title("Ficha Clínica")
    ficha.geometry("1600x900")
    ficha.resizable(width=False, height=False)
    ficha.config(bg='white')

    ficha.mainloop()

#CRUD
def VerArchivos():
    pass

def EditarArchivo():
    pass

def SubirArchivos():
    pass

def BorrarArchivos():
    pass

#Botón de Ficha Clínica
BotonFicha = ttk.Button(chat, text ="FICHA CLINICA", command=FichaClinica)
#,borderwidth=0, fg='#ffffff', bg='#5AC2A9', font=("Verdana",12) - Configuración de Boton no ttk
BotonFicha.place(x=390, y=50, height=30, width = 130)

#Botón Ver PDF
Boton_pdf = ttk.Button(chat, text ="Ver Archivos", command=VerArchivos)
#,borderwidth=0, fg='#ffffff', bg='#5AC2A9', font=("Verdana",12) - Configuración de Boton no ttk
Boton_pdf.place(x=550, y=50, height=30, width = 130)

chat.mainloop()
