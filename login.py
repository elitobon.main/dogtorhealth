class userclass():

    numUser = 0

    def __init__(self, name, pw):
        self.name = name
        self.pw = pw
        self.conectado = False
        self.intentos = 3

        userclass.numUser+=1

    def connect(self, pwd=None):
        if pwd == None:
            mypw = input("Ingrese su contraseña: ")
        else:
            mypw=pwd
        if mypw==self.pw:
            print("Se ha conectado con éxito!!")
            self.conectado = True
            return True
        else:
            self.intentos-=1
            if self.intentos > 0:
                print("Contraseña incorrecta, inténtelo de nuevo")
                if pwd!=None:
                    return False
                print("Intentos restantes ", self.intentos)
                self.conectado = False
            else:
                print("Error, no se pudo iniciar sesión")

    def disconnet(self):
        if self.connect:
            print("Se ha cerrado con éxito")
            self.connect = False
        else:
            print("Error, no hay un inicio de sesión abierto")

    def __str__(self):
        if self.connect:
            conect = "Conectado"
        else:
            conect = "Desconectado"
        
        return f"Mi nombre de usuario es {self.name}  y estoy  {conect}"

                            